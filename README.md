# Air Traffic Control
***
This project is done as part of the "Za rączkę" mentoring programme.
 

*Work in progress*


***
notes

#### Założenia techniczne.

- Aplikacja w architekturze klient/serwer. Komunikacja oparta na socketach.
- Informacje o nawiązanych połączeniach, kolizjach i udanych lądowaniach zapisujesz w bazie.
- Samoloty pełnią funkcję klientów.
- Wieża kontrolna pełni funkcję serwera.

#### Założenia biznesowe

- Lotnisko posiada 2 pasy
- W przestrzeni powietrznej lotniska może się znajdować maksymalnie 100 samolotów, każdy kolejny, w trakcie próby połączenia powinien otrzymać informację o konieczności znalezienia sobie innego lotniska i znika z twojego "radaru"
- Lotnisko zawiaduje przestrzenią 10 km na 10 km i wysokości 5 km.
- Każdy przylatujący samolot ma paliwa na 3 godziny lotu
- Należy ustalić korytarze powietrzne służące do lądowania
- Korytarz powietrzny to obszar w powietrzu, na którym ma prawo w danym momencie znajdować się tylko jeden kierowany do lądowania samolot.
- Samoloty pojawiają się w losowym miejscu na granicy obszaru powietrznego, POZA korytarzami powietrznymi na wysokości 2km do 5km
- Sprowadzenie samolotu na ziemię polega na skierowaniu go do korytarza powietrznego i pilnowania jego czystej drogi do pasa startowego, na pasie startowym samolot ma mieć wysokość równą 0m inaczej lądowanie jest uznane za nieudane i następuje kolizja
- Jeśli dwa samoloty znajdą się w odległości 10m od siebie, uznajemy, że nastąpiła kolizja
- Jeśli w samolocie podczas oczekiwania skończy się paliwo, uznajemy, że nastąpiła kolizja
Twoim zadaniem jest stworzenie systemu, który bezpiecznie będzie sprowadzał wszystkie samoloty na ziemię :)

