package pl.uncleglass.airtrafficcontrol.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.uncleglass.airtrafficcontrol.Property;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.catchThrowable;

class AltitudeTest {

    @ParameterizedTest
    @MethodSource("outOfRange")
    @DisplayName("Should throw an exception when created with out of range value")
    void shouldThrowAnExceptionWhenCreatedWithOutOfRangeValue(int value) {
        Throwable thrown = catchThrowable(() -> Altitude.of(value));

        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Value of height is out of the range");
    }

    private static Stream<Arguments> outOfRange() {
        return Stream.of(
                Arguments.of(Property.MIN_HEIGHT - 100),
                Arguments.of(Property.MIN_HEIGHT - 1),
                Arguments.of(Property.MAX_HEIGHT + 1),
                Arguments.of(Property.MAX_HEIGHT + 5001)
        );
    }

    @ParameterizedTest
    @MethodSource("inRange")
    @DisplayName("Should not throw an exception when created with in the range value")
    void shouldNotThrowAnExceptionWhenCreatedWithInTheRangeValue(int value) {
        assertThatCode(() -> Altitude.of(value))
                .doesNotThrowAnyException();
    }

    private static Stream<Arguments> inRange() {
        return Stream.of(
                Arguments.of(Property.MIN_HEIGHT),
                Arguments.of(Property.MIN_HEIGHT+1),
                Arguments.of(Property.MAX_HEIGHT),
                Arguments.of(Property.MAX_HEIGHT-1)
        );
    }
}