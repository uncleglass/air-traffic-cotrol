package pl.uncleglass.airtrafficcontrol.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.uncleglass.airtrafficcontrol.Property;
import pl.uncleglass.airtrafficcontrol.shared.Latitude;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.catchThrowable;

class LatitudeTest {
    @ParameterizedTest
    @MethodSource("outOfRange")
    @DisplayName("Should throw an exception when created with out of range value")
    void shouldThrowAnExceptionWhenCreatedWithOutOfRangeValue(int value) {
        Throwable thrown = catchThrowable(() -> Latitude.of(value));

        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Value of latitude is out of the range");
    }

    private static Stream<Arguments> outOfRange() {
        return Stream.of(
                Arguments.of(Property.MIN_LATITUDE - 100),
                Arguments.of(Property.MIN_LATITUDE - 1),
                Arguments.of(Property.MAX_LATITUDE + 1),
                Arguments.of(Property.MAX_LATITUDE + 2546)
        );
    }

    @ParameterizedTest
    @MethodSource("inRange")
    @DisplayName("Should not throw an exception when created with in the range value")
    void shouldNotThrowAnExceptionWhenCreatedWithInTheRangeValue(int value) {
        assertThatCode(() -> Latitude.of(value))
                .doesNotThrowAnyException();
    }

    private static Stream<Arguments> inRange() {
        return Stream.of(
                Arguments.of(Property.MIN_LATITUDE),
                Arguments.of(Property.MIN_LATITUDE + 1),
                Arguments.of(Property.MAX_LATITUDE),
                Arguments.of(Property.MAX_LATITUDE - 1)
        );
    }
}