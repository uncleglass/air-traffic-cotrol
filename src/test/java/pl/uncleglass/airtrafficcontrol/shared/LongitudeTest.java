package pl.uncleglass.airtrafficcontrol.shared;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import pl.uncleglass.airtrafficcontrol.Property;
import pl.uncleglass.airtrafficcontrol.shared.Longitude;

import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatCode;
import static org.assertj.core.api.Assertions.catchThrowable;

class LongitudeTest {
    @ParameterizedTest
    @MethodSource("outOfRange")
    @DisplayName("Should throw an exception when created with out of range value")
    void shouldThrowAnExceptionWhenCreatedWithOutOfRangeValue(int value) {
        Throwable thrown = catchThrowable(() -> Longitude.of(value));

        assertThat(thrown)
                .isInstanceOf(IllegalArgumentException.class)
                .hasMessage("Value of longitude is out of the range");
    }

    private static Stream<Arguments> outOfRange() {
        return Stream.of(
                Arguments.of(Property.MIN_LONGITUDE - 100),
                Arguments.of(Property.MIN_LONGITUDE - 1),
                Arguments.of(Property.MAX_LONGITUDE + 1),
                Arguments.of(Property.MAX_LONGITUDE + 2500)
        );
    }

    @ParameterizedTest
    @MethodSource("inRange")
    @DisplayName("Should not throw an exception when created with in the range value")
    void shouldNotThrowAnExceptionWhenCreatedWithInTheRangeValue(int value) {
        assertThatCode(() -> Longitude.of(value))
                .doesNotThrowAnyException();
    }

    private static Stream<Arguments> inRange() {
        return Stream.of(
                Arguments.of(Property.MIN_LONGITUDE),
                Arguments.of(Property.MAX_LONGITUDE),
                Arguments.of(Property.MIN_LONGITUDE + 1),
                Arguments.of(Property.MAX_LONGITUDE - 1)
        );
    }
}