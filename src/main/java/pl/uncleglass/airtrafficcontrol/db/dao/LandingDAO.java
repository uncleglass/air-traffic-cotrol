package pl.uncleglass.airtrafficcontrol.db.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.airtrafficcontrol.db.DBManager;
import pl.uncleglass.airtrafficcontrol.db.dto.LandingDTO;
import pl.uncleglass.airtrafficcontrol.db.dto.PlaneDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class LandingDAO {
    private static final Logger logger = LogManager.getLogger(LandingDAO.class);

    public static void save(LandingDTO landingToSave) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement("INSERT INTO landings(plane_id, landing_time) VALUES(?,?)")) {
            statement.setLong(1, landingToSave.planeId);
            statement.setTimestamp(2, Timestamp.valueOf(landingToSave.landingTime));
            statement.executeUpdate();
            logger.info("Landing was saved: {}", landingToSave);
        } catch (SQLException e) {
            logger.error("Something went wrong: {}", e.getMessage());
            logger.error(e.getStackTrace());
        }
    }

    public static List<LandingDTO> getAllLanded() {
        List<LandingDTO> landingDTOList = new ArrayList<>();
        String sql = "select l.id, l.plane_id, p.plane_name, l.landing_time from landings l inner join plane p on p.id = l.plane_id";
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                landingDTOList.add(
                        LandingDTO.of(
                                resultSet.getLong("id"),
                                resultSet.getLong("plane_id"),
                                resultSet.getString("plane_name"),
                                resultSet.getObject("landing_time", LocalDateTime.class)
                        )
                );
            }
        } catch (SQLException e) {
            logger.error("Something went wrong: {}", e.getMessage());
        }
        return landingDTOList;
    }
}
