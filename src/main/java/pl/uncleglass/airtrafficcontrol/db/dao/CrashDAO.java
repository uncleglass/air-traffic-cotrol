package pl.uncleglass.airtrafficcontrol.db.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.airtrafficcontrol.db.DBManager;
import pl.uncleglass.airtrafficcontrol.db.dto.CrashDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class CrashDAO {
    public static final Logger logger = LogManager.getLogger(CrashDAO.class);
    private static final String SAVE_COLLISION_SQL = "INSERT INTO crashes(plane_1_id, plane_2_id, plane_1_position, plane_2_position, time, reason) VALUES (?,?,?,?,?,?)";
    private static final String SAVE_CRASH_SQL = "INSERT INTO crashes(plane_1_id, plane_1_position, time, reason) VALUES (?,?,?,?)";

    public static void saveCollision(CrashDTO crashToSave) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SAVE_COLLISION_SQL)) {
            statement.setLong(1, crashToSave.planeOneId);
            statement.setLong(2, crashToSave.planeTwoId);
            statement.setString(3, crashToSave.planeOnePosition);
            statement.setString(4, crashToSave.planeTwoPosition);
            statement.setTimestamp(5, Timestamp.valueOf(crashToSave.time));
            statement.setString(6, String.valueOf(crashToSave.crashReason));
            statement.execute();
            logger.info("Collision was saved: {}", crashToSave);
        } catch (SQLException e) {
            logger.error("Something went wrong: {}", e.getMessage());
            logger.error(e.getStackTrace());
        }
    }

    public static void saveCrash(CrashDTO crashToSave) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SAVE_CRASH_SQL)) {
            statement.setLong(1, crashToSave.planeOneId);
            statement.setString(2, crashToSave.planeOnePosition);
            statement.setTimestamp(3, Timestamp.valueOf(crashToSave.time));
            statement.setString(4, String.valueOf(crashToSave.crashReason));
            statement.execute();
            logger.info("Crash was saved: {}", crashToSave);
        } catch (SQLException e) {
            logger.error("Something went wrong: {}", e.getMessage());
            logger.error(e.getStackTrace());
        }
    }

    public static List<CrashDTO> getAll() {
        List<CrashDTO> crashesDTOList = new ArrayList<>();
        String sql = """
                select c.id,
                       c.plane_1_id plane1_id,
                       p1.plane_name plane1_name,
                       c.plane_2_id plane2_id,
                       p2.plane_name plane2_name,
                       c.plane_1_position plane1_position,
                       c.plane_2_position plane2_position,
                       c.time,
                       c.reason
                from crashes c
                         inner join plane p1
                                    on p1.id = c.plane_1_id
                         inner join plane p2
                                    on p2.id = c.plane_2_id
                """;
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)
        ) {
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                crashesDTOList.add(
                        CrashDTO.of(
                                resultSet.getLong("id"),
                                resultSet.getLong("plane1_id"),
                                resultSet.getString("plane1_name"),
                                resultSet.getLong("plane2_id"),
                                resultSet.getString("plane2_name"),
                                resultSet.getString("plane1_position"),
                                resultSet.getString("plane2_position"),
                                resultSet.getObject("time", LocalDateTime.class),
                                CrashDTO.CrashReason.valueOf(resultSet.getString("reason"))
                        )

                );
            }
        } catch (SQLException e) {
            logger.error("Something went wrong: {}", e.getMessage());
        }
        return crashesDTOList;
    }
}
