package pl.uncleglass.airtrafficcontrol.db.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.airtrafficcontrol.db.DBManager;
import pl.uncleglass.airtrafficcontrol.db.dto.PlaneDTO;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class PlaneDAO {
    private static final Logger logger = LogManager.getLogger(PlaneDAO.class);
    private static final String SAVE_PLANE_SQL = "INSERT INTO plane(plane_name, first_contact_time, served) VALUES (?,?,?)";

    private PlaneDAO() {
    }

    public static PlaneDTO savePlane(PlaneDTO plane) {
        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SAVE_PLANE_SQL, Statement.RETURN_GENERATED_KEYS)) {
            statement.setString(1, plane.planeName);
            statement.setTimestamp(2, Timestamp.valueOf(plane.firstContactTime));
            statement.setBoolean(3, plane.served);
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            generatedKeys.next();
            long id = generatedKeys.getLong(1);
            PlaneDTO savedPlane = PlaneDTO.of(id, plane.planeName, plane.firstContactTime, plane.served);
            logger.info("Plane was saved: {}", savedPlane);
            return savedPlane;
        } catch (SQLException e) {
            logger.error("Plane saving failed: {}" ,plane);
            logger.error("Something went wrong: {}" , e.getMessage());
            logger.error(e.getStackTrace());
            return plane;
        }
    }

    public static PlaneDTO getInfo(Long id) {
        String sql = "select id, plane_name, first_contact_time, served from plane where id = ?";

        try (Connection connection = DBManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            statement.setLong(1, id);

            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return PlaneDTO.of(
                        resultSet.getLong("id"),
                        resultSet.getString("plane_name"),
                        resultSet.getObject("first_contact_time", LocalDateTime.class),
                        resultSet.getBoolean("served")
                );
            }

        } catch (SQLException e) {
        }
        return null;
    }
}
