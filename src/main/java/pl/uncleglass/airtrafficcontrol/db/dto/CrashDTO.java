package pl.uncleglass.airtrafficcontrol.db.dto;

import java.time.LocalDateTime;

public class CrashDTO {
    public  enum CrashReason {COLLISION, FUEL_RUN_OUT}

    public final Long id;
    public final Long planeOneId;
    public final String planeOneName;
    public final Long planeTwoId;
    public final String planeTwoName;

    public final String planeOnePosition;
    public final String planeTwoPosition;
    public final LocalDateTime time;
    public final CrashReason crashReason;

    private CrashDTO(Long id, Long planeOneId, String planeOneName, Long planeTwoId, String planeTwoName, String planeOnePosition, String planeTwoPosition, LocalDateTime time, CrashReason crashReason) {
        this.id = id;
        this.planeOneId = planeOneId;
        this.planeOneName = planeOneName;
        this.planeTwoId = planeTwoId;
        this.planeTwoName = planeTwoName;
        this.planeOnePosition = planeOnePosition;
        this.planeTwoPosition = planeTwoPosition;
        this.time = time;
        this.crashReason = crashReason;
    }

    public static CrashDTO of(Long id, Long planeOneId, String planeOneName, Long planeTwoId, String planeTwoName, String planeOnePosition, String planeTwoPosition, LocalDateTime time, CrashReason crashReason) {
        return new CrashDTO(id, planeOneId, planeOneName, planeTwoId, planeTwoName, planeOnePosition, planeTwoPosition, time, crashReason);
    }

    public static CrashDTO of(Long planeOneId, Long planeTwoId, String planeOnePosition, String planeTwoPosition, LocalDateTime time) {
        return of(null, planeOneId, null, planeTwoId, null, planeOnePosition, planeTwoPosition, time, CrashReason.COLLISION);
    }

    public static CrashDTO of(Long planeOneId, String planeOnePosition, LocalDateTime time) {
        return of(null, planeOneId, null, null, null, planeOnePosition, null, time, CrashReason.FUEL_RUN_OUT);
    }

    @Override
    public String toString() {
        return "CrashDTO{" +
                "id=" + id +
                ", planeOneId=" + planeOneId +
                ", planeTwoId=" + planeTwoId +
                ", planeOnePosition='" + planeOnePosition + '\'' +
                ", planeTwoPosition='" + planeTwoPosition + '\'' +
                ", time=" + time +
                ", crashReason=" + crashReason +
                '}';
    }
}
