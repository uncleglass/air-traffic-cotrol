package pl.uncleglass.airtrafficcontrol.db.dto;

import java.time.LocalDateTime;

public class PlaneDTO {
    public final Long id;
    public final String planeName;
    public final LocalDateTime firstContactTime;
    public final Boolean served;

    private PlaneDTO(Long id, String planeName, LocalDateTime firstContactTime, Boolean served) {
        this.id = id;
        this.planeName = planeName;
        this.firstContactTime = firstContactTime;
        this.served = served;
    }

    public static PlaneDTO of(String planeName, LocalDateTime firstContactTime, Boolean served) {
        return of(null, planeName, firstContactTime, served);
    }

    public static PlaneDTO of(Long id, String planeName, LocalDateTime firstContactTime, Boolean served) {
        return new PlaneDTO(id, planeName, firstContactTime, served);
    }

    @Override
    public String toString() {
        return "PlaneDTO{" +
                "id=" + id +
                ", planeName='" + planeName + '\'' +
                ", firstContactTime=" + firstContactTime +
                ", served=" + served +
                '}';
    }
}
