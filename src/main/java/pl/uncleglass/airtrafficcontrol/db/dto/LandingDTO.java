package pl.uncleglass.airtrafficcontrol.db.dto;

import java.time.LocalDateTime;

public class LandingDTO {
    public final Long id;
    public final Long planeId;
    public final String planeName;
    public final LocalDateTime landingTime;

    private LandingDTO(Long id, Long planeId, String planeName, LocalDateTime landingTime) {
        this.id = id;
        this.planeId = planeId;
        this.planeName = planeName;
        this.landingTime = landingTime;
    }

    public static LandingDTO of(Long planeId, LocalDateTime landingTime) {
        return of(null, planeId, null, landingTime);
    }

    public static LandingDTO of(Long id, Long planeId, String planeName, LocalDateTime landingTime) {
        return new LandingDTO(id, planeId, planeName, landingTime);
    }

    @Override
    public String toString() {
        return "LandingDTO{" +
                "planeId=" + planeId +
                ", landingTime=" + landingTime +
                '}';
    }
}
