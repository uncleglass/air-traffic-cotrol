package pl.uncleglass.airtrafficcontrol.db;

import com.zaxxer.hikari.HikariDataSource;

import java.sql.Connection;
import java.sql.SQLException;

public class DBManager {
    private static final HikariDataSource dataSource;

    static {
        dataSource = new HikariDataSource();
        dataSource.setJdbcUrl("jdbc:postgresql://localhost:5432/postgres");
        dataSource.setUsername("postgres");
        dataSource.setPassword("bartek");
    }

    public static Connection getConnection() throws SQLException {
        return dataSource.getConnection();
    }

    private static void closeDBConnectionPool() {
        dataSource.close();
    }
}
