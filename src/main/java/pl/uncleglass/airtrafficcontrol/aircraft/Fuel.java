package pl.uncleglass.airtrafficcontrol.aircraft;

public final class Fuel {
    public final int value;

    private Fuel(int value) {
        this.value = value;
    }

    public static Fuel of(int fuelQuantity) {
        return new Fuel(fuelQuantity);
    }

    public Fuel burn() {
        return of(value - 1);
    }

    public boolean isRunOut() {
        return value == 0;
    }
}
