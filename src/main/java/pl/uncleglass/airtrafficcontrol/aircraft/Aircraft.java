package pl.uncleglass.airtrafficcontrol.aircraft;

import pl.uncleglass.airtrafficcontrol.shared.ConnectionHandler;
import pl.uncleglass.airtrafficcontrol.shared.JsonConverter;
import pl.uncleglass.airtrafficcontrol.shared.Position;
import pl.uncleglass.airtrafficcontrol.shared.Request;
import pl.uncleglass.airtrafficcontrol.shared.Response;

import java.io.IOException;
import java.net.Socket;
import java.util.List;
import java.util.Timer;

import static pl.uncleglass.airtrafficcontrol.Property.SECTOR_IN_METERS;
import static pl.uncleglass.airtrafficcontrol.Property.SERVER_ADDRESS;
import static pl.uncleglass.airtrafficcontrol.Property.SERVER_PORT;
import static pl.uncleglass.airtrafficcontrol.Property.SPEED_IN_METERS_PER_SEC;
import static pl.uncleglass.airtrafficcontrol.Property.TIME_UNIT;

public class Aircraft implements Runnable {
    private final String name;
    private final Piloting piloting;
    private final Tank tank = new Tank();
    private ConnectionHandler groundControlConnection;
    private final Timer engine = new Timer("Timer");

    public Aircraft(String name, Position startPosition) {
        this.name = name;
        this.piloting = new Piloting(startPosition);
        engine.scheduleAtFixedRate(tank, 0, TIME_UNIT);
        engine.scheduleAtFixedRate(piloting, 0, TIME_UNIT / (SPEED_IN_METERS_PER_SEC / SECTOR_IN_METERS));
    }

    @Override
    public void run() {
        try {
            groundControlConnection = new ConnectionHandler(new Socket(SERVER_ADDRESS, SERVER_PORT));
        } catch (IOException e) {
            e.printStackTrace();
        }
        String hi = JsonConverter.to(Request.hi(name));
        groundControlConnection.send(hi);

        while (true) {
            Response response = JsonConverter.from(groundControlConnection.receive(), Response.class);

            if (response.isFreeze()) {
                piloting.freeze();
                tank.freeze();
                String freezeRequest = JsonConverter.to(Request.freeze(name));
                groundControlConnection.send(freezeRequest);
            }

            if (response.isThaw()) {
                piloting.thaw();
                tank.thaw();
                String thawRequest = JsonConverter.to(Request.thaw(name));
                groundControlConnection.send(thawRequest);
            }

            if (response.isNoSpace() ||
                    response.isRunOutOfFuel() ||
                    response.isHasLanded() ||
                    response.isCollision() ||
                    response.isStop()) {
                groundControlConnection.close();
                engine.cancel();
                break;
            }

            if (response.isPosition()) {
                if (tank.isRunOutOfFuel()) {
                    String runOut = JsonConverter.to(Request.runOutOfFuel(name));
                    groundControlConnection.send(runOut);
                } else {
                    String aircraftPosition = JsonConverter.to(Request.position(
                                    name,
                                    piloting.getCurrentPosition(),
                                    piloting.isNoMoreDestination(),
                                    tank.getFuel()
                            )
                    );
                    groundControlConnection.send(aircraftPosition);
                }
            }

            if (response.isNewDestination()) {
                List<Position> destination = response.getDestination();
                piloting.setNewDestination(destination);
                String newDirectionRequest = JsonConverter.to(Request.newDirection(name));
                groundControlConnection.send(newDirectionRequest);
            }

            if (response.isDestination()) {
                List<Position> destination = response.getDestination();
                piloting.setDestination(destination);
                String directionRequest = JsonConverter.to(Request.direction(name));
                groundControlConnection.send(directionRequest);
            }
        }
        System.out.println("close" + name);
    }
}