package pl.uncleglass.airtrafficcontrol.aircraft;

import pl.uncleglass.airtrafficcontrol.shared.FlyDirection;
import pl.uncleglass.airtrafficcontrol.shared.Latitude;
import pl.uncleglass.airtrafficcontrol.shared.Longitude;
import pl.uncleglass.airtrafficcontrol.shared.Position;

import java.util.Collections;
import java.util.List;

import static pl.uncleglass.airtrafficcontrol.Property.MAX_LATITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_LONGITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MIN_LATITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MIN_LONGITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.TURN_POINT;
import static pl.uncleglass.airtrafficcontrol.shared.FlyDirection.LAT_DOWN;
import static pl.uncleglass.airtrafficcontrol.shared.FlyDirection.LAT_UP;
import static pl.uncleglass.airtrafficcontrol.shared.FlyDirection.LON_DOWN;
import static pl.uncleglass.airtrafficcontrol.shared.FlyDirection.LON_UP;

public class DestinationProvider {

    public static List<Position> get(Position actualPosition, Position lastPosition) {
        Position position = calculateDestination(actualPosition, lastPosition);
        return Collections.singletonList(position);
    }

    private static Position calculateDestination(Position actualPosition, Position lastPosition) {
        Latitude latitude = actualPosition.latitude;
        Longitude longitude = actualPosition.longitude;
        if (lastPosition == null) {
            if (latitude.value == MIN_LATITUDE) {
                latitude = Latitude.of(MAX_LATITUDE - TURN_POINT);
            } else if (latitude.value == MAX_LATITUDE) {
                latitude = Latitude.of(MIN_LATITUDE + TURN_POINT);
            } else if (longitude.value == MIN_LONGITUDE) {
                longitude = Longitude.of(MAX_LONGITUDE - TURN_POINT);
            } else if (longitude.value == MAX_LONGITUDE) {
                longitude = Longitude.of(MIN_LONGITUDE + TURN_POINT);
            }
        } else {
            FlyDirection flyDirection = FlyDirection.check(actualPosition, lastPosition);
            if (aircraftReachedTurnPoint(actualPosition, flyDirection)) {
                switch (flyDirection) {
                    case LAT_UP:
                        latitude = latitude.add(34);
                        int half = MAX_LONGITUDE / 2;
                        longitude = longitude.value > half ? longitude.subtract(36) : longitude.add(36);
                        break;
                    case LAT_DOWN:
                        latitude = latitude.subtract(34);
                        half = MAX_LONGITUDE / 2;
                        longitude = longitude.value > half ? longitude.subtract(36) : longitude.add(36);
                        break;
                    case LON_UP:
                        longitude = longitude.add(34);
                        half = MAX_LATITUDE / 2;
                        latitude = latitude.value > half ? latitude.subtract(36) : latitude.add(36);
                        break;
                    case LON_DOWN:
                        longitude = longitude.subtract(34);
                        half = MAX_LATITUDE / 2;
                        latitude = latitude.value > half ? latitude.subtract(36) : latitude.add(36);
                }
            } else {
                switch (flyDirection) {
                    case LAT_UP:
                        latitude = Latitude.of(MAX_LATITUDE - TURN_POINT);
                        break;
                    case LAT_DOWN:
                        latitude = Latitude.of(MIN_LATITUDE + TURN_POINT);
                        break;
                    case LON_UP:
                        longitude = Longitude.of(MAX_LONGITUDE - TURN_POINT);
                        break;
                    case LON_DOWN:
                        longitude = Longitude.of(MIN_LONGITUDE + TURN_POINT);
                        break;
                }
            }
        }
        return Position.of(latitude, longitude, actualPosition.altitude);
    }

    private static boolean aircraftReachedTurnPoint(Position actualPosition, FlyDirection flyDirection) {
        Latitude actualLatitude = actualPosition.latitude;
        Longitude actualLongitude = actualPosition.longitude;
        return Latitude.of(MAX_LATITUDE - TURN_POINT).equals(actualLatitude)
                && flyDirection == LAT_UP
                || Latitude.of(MIN_LATITUDE + TURN_POINT).equals(actualLatitude)
                && flyDirection == LAT_DOWN
                || Longitude.of(MAX_LONGITUDE - TURN_POINT).equals(actualLongitude)
                && flyDirection == LON_UP
                || Longitude.of(MIN_LONGITUDE + TURN_POINT).equals(actualLongitude)
                && flyDirection == LON_DOWN;
    }
}
