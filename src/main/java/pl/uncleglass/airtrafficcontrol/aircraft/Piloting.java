package pl.uncleglass.airtrafficcontrol.aircraft;

import pl.uncleglass.airtrafficcontrol.shared.Position;

import java.util.List;
import java.util.Queue;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentLinkedQueue;

public class Piloting extends TimerTask {
    private Position currentPosition;
    private Position previousPosition;
    private Queue<Position> destinations = new ConcurrentLinkedQueue<>();
    private Position currentDestination;
    private List<Position> destinationToApply;
    private boolean applyNewDestination = false;
    private boolean noMoreDestination = false;
    private boolean freeze;

    public Piloting(Position startPosition) {
        this.currentPosition = startPosition;
    }

    @Override
    public void run() {
        if (freeze) {
            return;
        }
        if (applyNewDestination) {
            currentDestination = null;
            destinations.clear();
            destinationToApply.forEach(destinations::offer);
            noMoreDestination = false;
            applyNewDestination = false;
        }
        if (destinations.isEmpty() && currentDestination == null) {
            List<Position> destination = DestinationProvider.get(currentPosition, previousPosition);
            destination.forEach(destinations::offer);

        }
        if (currentDestination == null) {
            currentDestination = destinations.poll();
            if (destinations.isEmpty()) {
                noMoreDestination = true;
            }
        }
        previousPosition = currentPosition;
        currentPosition = currentPosition.makeCloser(currentDestination);
        if (currentPosition.reached(currentDestination)) {
            currentDestination = null;
        }
    }

    public Position getCurrentPosition() {
        return currentPosition;
    }

    public void setNewDestination(List<Position> destination) {
        destinationToApply = destination;
        applyNewDestination = true;
    }

    public boolean isNoMoreDestination() {
        return noMoreDestination;
    }

    public void setDestination(List<Position> destination) {
        noMoreDestination = false;
        destination.forEach(destinations::offer);
    }

    public void freeze() {
        freeze = true;
    }

    public void thaw() {
        freeze = false;
    }
}
