package pl.uncleglass.airtrafficcontrol.aircraft;

import java.util.TimerTask;

import static pl.uncleglass.airtrafficcontrol.Property.FUEL_QUANTITY;

public class Tank extends TimerTask {
    private Fuel fuel = Fuel.of(FUEL_QUANTITY);
    private boolean freeze;

    @Override
    public void run() {
        if (freeze) {
            return;
        }
        fuel = fuel.burn();
        if (fuel.isRunOut()) {
            cancel();
        }
    }

    public boolean isRunOutOfFuel() {
        return fuel.isRunOut();
    }

    public Fuel getFuel() {
        return fuel;
    }

    public void freeze() {
        freeze = true;
    }

    public void thaw() {
        freeze = false;
    }
}
