package pl.uncleglass.airtrafficcontrol.aircraft;

import pl.uncleglass.airtrafficcontrol.shared.Position;

import java.util.Random;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import static pl.uncleglass.airtrafficcontrol.Property.MAX_HEIGHT;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_LATITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_LONGITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MIN_APPEARANCE_HEIGHT;
import static pl.uncleglass.airtrafficcontrol.Property.MIN_LATITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MIN_LONGITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.TIME_UNIT;

public class AircraftGenerator implements Runnable {
    private final ExecutorService executorService = Executors.newFixedThreadPool(110, r -> new Thread(r, "aircraft"));
    private final Random random = new Random();
    private boolean freeze;
    private boolean run;

    @Override
    public void run() {
        run = true;
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Aircraft generator starts working...");

        while (run) {
            if (!freeze) {
                executorService.execute(new Aircraft(
                        generateName(),
                        generatePosition()
                ));
            }

            try {
                TimeUnit.MILLISECONDS.sleep(TIME_UNIT * 60);
            } catch (InterruptedException e) {
//                e.printStackTrace();
                //TODO log it
            }

        }
        System.out.println("Aircraft generator stops working...");
    }

    private String generateName() {
        return generateAlphabeticalPart() + generateNumericalPart();
    }

    private String generateAlphabeticalPart() {
        char leftLimit = 'A';
        char rightLimit = 'Z';
        return random.ints(leftLimit, rightLimit + 1)
                .limit(3)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private String generateNumericalPart() {
        return random.ints(0, 9)
                .limit(4)
                .collect(StringBuilder::new, StringBuilder::append, StringBuilder::append)
                .toString();
    }

    private Position generatePosition() {
        int randomLongitude = randomFromRange(MIN_LONGITUDE, MAX_LONGITUDE);
        int randomLatitude = randomFromRange(MIN_LATITUDE, MAX_LATITUDE);
        int randomHeight = randomFromRange(MIN_APPEARANCE_HEIGHT, MAX_HEIGHT);
        int border = randomFromRange(1, 5);
        return switch (border) {
            case 1 -> Position.of(randomLatitude, MIN_LONGITUDE, randomHeight);
            case 2 -> Position.of(MAX_LATITUDE, randomLongitude, randomHeight);
            case 3 -> Position.of(randomLatitude, MAX_LONGITUDE, randomHeight);
            case 4 -> Position.of(MIN_LATITUDE, randomLongitude, randomHeight);
            default -> throw new IllegalStateException();
        };
    }

    private int randomFromRange(int min, int max) {
        return random.ints(min, max)
                .findFirst()
                .getAsInt();
    }

    public void freeze() {
       freeze = true;
    }

    public void thaw() {
        freeze = false;
    }

    public void stop() {
        run = false;
    }
}
