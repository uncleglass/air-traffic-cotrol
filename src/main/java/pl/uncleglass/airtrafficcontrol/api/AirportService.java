package pl.uncleglass.airtrafficcontrol.api;

import org.springframework.stereotype.Service;
import pl.uncleglass.airtrafficcontrol.AirTrafficControlController;

@Service
public class AirportService {
    private final AirTrafficControlController airTrafficControlApp;

    public AirportService(AirTrafficControlController airTrafficControlApp) {
        this.airTrafficControlApp = airTrafficControlApp;
    }

    public boolean start() {
        return airTrafficControlApp.start();
    }

    public long getUpTime() {
        return airTrafficControlApp.getUpTimeInSeconds();
    }

    public void freeze() {
        airTrafficControlApp.freeze();
    }

    public void thaw() {
        airTrafficControlApp.thaw();
    }

    public void stop() {
        airTrafficControlApp.stop();
    }
}
