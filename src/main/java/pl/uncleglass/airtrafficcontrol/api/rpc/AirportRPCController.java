package pl.uncleglass.airtrafficcontrol.api.rpc;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping
public class AirportRPCController {

    private final RPCService rpcService;


    public AirportRPCController(RPCService rpcService) {
        this.rpcService = rpcService;
    }

    @PostMapping("/ControlAirport")
    public String controlAirport(@RequestBody AirportCommand airportCommand) {
        return rpcService.controlAirport(airportCommand);
    }

    @PostMapping("/Info")
    public String controlAirport(@RequestBody InfoCommand infoCommand) {
        return rpcService.getInfo(infoCommand);
    }
}
