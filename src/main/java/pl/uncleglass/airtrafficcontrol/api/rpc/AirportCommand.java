package pl.uncleglass.airtrafficcontrol.api.rpc;

public class AirportCommand {
    public enum Command {
        START, STOP, FREEZE, THAW
    }


    private Command command;

    private void setCommand(Command command) {
        this.command = command;
    }

    public boolean isStart() {
        return command == Command.START;
    }

    public boolean isStop() {
        return command == Command.STOP;
    }

    public boolean isFreeze() {
        return command == Command.FREEZE;
    }

    public boolean isThaw() {
        return command == Command.THAW;
    }
}
