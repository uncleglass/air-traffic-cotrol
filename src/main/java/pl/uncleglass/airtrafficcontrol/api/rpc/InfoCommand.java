package pl.uncleglass.airtrafficcontrol.api.rpc;

public class InfoCommand {
    public enum Command {
        CRASHES, UPTIME, LANDINGS, COUNT, IDS, PLANE
    }

    private Command command;
    private long planeId;

    private void setCommand(Command command) {
        this.command = command;
    }

    private void setPlaneId(int planeId) {
        this.planeId = planeId;
    }

    public long getPlaneId() {
        return planeId;
    }

    public boolean isCrashes() {
        return command == Command.CRASHES;
    }

    public boolean isUPtime() {
        return command == Command.UPTIME;
    }

    public boolean isLandings() {
        return command == Command.LANDINGS;
    }

    public boolean isCount() {
        return command == Command.COUNT;
    }

    public boolean isIds() {
        return command == Command.IDS;
    }

    public boolean isPlane() {
        return command == Command.PLANE;
    }
}
