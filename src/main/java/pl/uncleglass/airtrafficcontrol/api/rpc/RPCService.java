package pl.uncleglass.airtrafficcontrol.api.rpc;

import org.springframework.stereotype.Service;
import pl.uncleglass.airtrafficcontrol.api.AirportService;
import pl.uncleglass.airtrafficcontrol.api.CrashesService;
import pl.uncleglass.airtrafficcontrol.api.LandingService;
import pl.uncleglass.airtrafficcontrol.api.PlaneService;
import pl.uncleglass.airtrafficcontrol.db.dto.CrashDTO;
import pl.uncleglass.airtrafficcontrol.db.dto.LandingDTO;

import java.util.stream.Collectors;

@Service
public class RPCService {

    private final AirportService airportService;
    private final CrashesService crashesService;
    private final LandingService landingService;
    private final PlaneService planeService;

    public RPCService(
            AirportService airportService,
            CrashesService crashesService,
            LandingService landingService,
            PlaneService planeService) {
        this.airportService = airportService;
        this.crashesService = crashesService;
        this.landingService = landingService;
        this.planeService = planeService;
    }

    public String controlAirport(AirportCommand airportCommand) {

        if (airportCommand.isStart()) {
            if (airportService.start()) {
                return "Airport has started";
            }
            return "Airport already runs";
        }

        if (airportCommand.isStop()) {
            airportService.stop();
            return "Airport stops.";
        }

        if (airportCommand.isFreeze()) {
            airportService.freeze();
            return "Airport freezes";
        }

        if (airportCommand.isThaw()) {
            airportService.thaw();
            return "Airport thaw";
        }

        return "Unknown command";
    }

    public String getInfo(InfoCommand infoCommand) {

        if (infoCommand.isCrashes()) {
            return  crashesService.getAll().stream()
                    .map(CrashDTO::toString)
                    .collect(Collectors.joining("\n"));
        }

        if (infoCommand.isUPtime()) {
            return "Uptime: " + airportService.getUpTime();
        }

        if (infoCommand.isLandings()) {
            return landingService.getAllLanded().stream()
                    .map(LandingDTO::toString)
                    .collect(Collectors.joining("\n"));
        }

        if (infoCommand.isCount()) {
            return "Planes count: " + planeService.getPlaneCount();
        }

        if (infoCommand.isIds()) {
            return String.join(", ", planeService.getPlanesIds());
        }

        if (infoCommand.isPlane()) {
            return planeService.getInfo(infoCommand.getPlaneId()).toString();
        }

        return "Unknown command";
    }
}
