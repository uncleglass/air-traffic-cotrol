package pl.uncleglass.airtrafficcontrol.api.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.uncleglass.airtrafficcontrol.api.PlaneService;
import pl.uncleglass.airtrafficcontrol.db.dto.PlaneDTO;

import java.util.List;

@RestController
@RequestMapping("/planes")
public class PlaneController {
    private final PlaneService planeService;

    public PlaneController(PlaneService planeService) {
        this.planeService = planeService;
    }

    @GetMapping("/count")
    public String getCount() {
        return planeService.getPlaneCount();
    }

    @GetMapping("/ids")
    public List<String> getPlanesIds() {
        return planeService.getPlanesIds();
    }

    @GetMapping("/{id}")
    public PlaneDTO getInfo(@PathVariable Long id) {
        return planeService.getInfo(id);
    }
}
