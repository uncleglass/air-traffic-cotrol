package pl.uncleglass.airtrafficcontrol.api.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.uncleglass.airtrafficcontrol.api.AirportService;

@RestController
@RequestMapping("/airport")
public class AirportController {
    private final AirportService airportService;

    public AirportController(AirportService airportService) {
        this.airportService = airportService;
    }

    @GetMapping("/start")
    public String start() {
        if (airportService.start()) {
            return "Airport has started";
        }
        return "Airport already has started.";
    }

    @GetMapping("/stop")
    public String stop() {
        airportService.stop();
        return "Airport is shutdown";
    }

    @GetMapping("/freeze")
    public String freeze() {
        airportService.freeze();
        return "freeze";
    }

    @GetMapping("/thaw")
    public String thaw() {
        airportService.thaw();
        return "thaw";
    }

    @GetMapping("/uptime")
    public String upTime() {
        return "Up time in seconds: " + airportService.getUpTime();
    }
}
