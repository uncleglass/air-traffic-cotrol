package pl.uncleglass.airtrafficcontrol.api.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.uncleglass.airtrafficcontrol.api.CrashesService;
import pl.uncleglass.airtrafficcontrol.db.dto.CrashDTO;

import java.util.List;

@RestController
@RequestMapping("/crashes")
public class CrashesController {
    private final CrashesService crashesService;

    public CrashesController(CrashesService crashesService) {
        this.crashesService = crashesService;
    }

    @GetMapping("/all")
    public List<CrashDTO> getAll() {
        return crashesService.getAll();
    }
}
