package pl.uncleglass.airtrafficcontrol.api.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import pl.uncleglass.airtrafficcontrol.api.LandingService;
import pl.uncleglass.airtrafficcontrol.db.dto.LandingDTO;

import java.util.List;

@RestController
@RequestMapping("/landings")
class LandingController {

    private final LandingService landingService;

    public LandingController(LandingService landingService) {
        this.landingService = landingService;
    }

    @GetMapping("/all")
    public List<LandingDTO> getAllLanded() {
        return landingService.getAllLanded();
    }
 }
