package pl.uncleglass.airtrafficcontrol.api;

import org.springframework.stereotype.Service;
import pl.uncleglass.airtrafficcontrol.AirTrafficControlController;
import pl.uncleglass.airtrafficcontrol.db.dao.PlaneDAO;
import pl.uncleglass.airtrafficcontrol.db.dto.PlaneDTO;

import java.util.List;

@Service
public class PlaneService {

    private final AirTrafficControlController airTrafficControlApp;

    public PlaneService(AirTrafficControlController airTrafficControlApp) {
        this.airTrafficControlApp = airTrafficControlApp;
    }

    public String getPlaneCount() {
        int planesCount = airTrafficControlApp.getPlanesCount();
        return "Planes count: " + planesCount;
    }

    public List<String> getPlanesIds() {
        return airTrafficControlApp.getPlanesIds();
    }

    public PlaneDTO getInfo(Long id) {
        return PlaneDAO.getInfo(id);
    }
}
