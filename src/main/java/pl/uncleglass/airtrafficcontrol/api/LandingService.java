package pl.uncleglass.airtrafficcontrol.api;

import org.springframework.stereotype.Service;
import pl.uncleglass.airtrafficcontrol.db.dao.LandingDAO;
import pl.uncleglass.airtrafficcontrol.db.dto.LandingDTO;

import java.util.List;

@Service
public class LandingService {

    public List<LandingDTO> getAllLanded() {
        return LandingDAO.getAllLanded();
    }
}
