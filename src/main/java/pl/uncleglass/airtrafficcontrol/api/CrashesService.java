package pl.uncleglass.airtrafficcontrol.api;

import org.springframework.stereotype.Service;
import pl.uncleglass.airtrafficcontrol.db.dao.CrashDAO;
import pl.uncleglass.airtrafficcontrol.db.dto.CrashDTO;
import pl.uncleglass.airtrafficcontrol.db.dto.LandingDTO;

import java.util.List;

@Service
public class CrashesService {

    public List<CrashDTO> getAll() {
        return CrashDAO.getAll();
    }
}
