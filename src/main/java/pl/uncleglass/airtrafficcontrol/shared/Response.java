package pl.uncleglass.airtrafficcontrol.shared;

import java.util.List;

import static pl.uncleglass.airtrafficcontrol.shared.Response.Status.*;

public class Response {


    public enum Status {
        OK, NO_SPACE, POSITION, RUN_OUT_OF_FUEL, NEW_DESTINATION, DESTINATION, HAS_LANDED, COLLISION, FREEZE, THAW, STOP


    }
    private final Status status;
    private final List<Position> destination;
    private Response(Status status, List<Position> destination) {
        this.status = status;
        this.destination = destination;
    }
    public static Response ok() {
        return new Response(OK, null);
    }

    public static Response noSpace() {
        return new Response(NO_SPACE, null);
    }
    public boolean isNoSpace() {
        return status == NO_SPACE;
    }
    public static Response position() {
        return new Response(POSITION, null);
    }
    public boolean isPosition() {
        return status == POSITION;
    }

    public static Response runOutOfFuel() {
        return new Response(RUN_OUT_OF_FUEL, null);
    }

    public boolean isRunOutOfFuel() {
        return status == RUN_OUT_OF_FUEL;
    }

    public static Response newDestination(List<Position> destination) {
        return new Response(NEW_DESTINATION, destination);
    }

    public boolean isNewDestination() {
        return status == NEW_DESTINATION;
    }

    public List<Position> getDestination() {
        return destination;
    }

    public static Response destination(List<Position> destination) {
        return new Response(DESTINATION, destination);
    }

    public boolean isDestination() {
        return status == DESTINATION;
    }

    public static Response hasLanded() {
        return new Response(HAS_LANDED, null);
    }

    public boolean isHasLanded() {
        return status == HAS_LANDED;
    }

    public static Response collision() {
        return new Response(COLLISION, null);
    }

    public boolean isCollision() {
        return status == COLLISION;
    }

    public static Response freeze() {
        return new Response(FREEZE, null);
    }

    public  boolean isFreeze() {
        return status == FREEZE;
    }

    public static Response thaw() {
        return new Response(THAW, null);
    }

    public boolean isThaw() {
        return status == THAW;
    }

    public static Response stop() {
        return new Response(STOP, null);
    }

    public boolean isStop() {
        return status == STOP;
    }
}
