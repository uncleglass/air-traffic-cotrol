package pl.uncleglass.airtrafficcontrol.shared;

import pl.uncleglass.airtrafficcontrol.aircraft.Fuel;

import static pl.uncleglass.airtrafficcontrol.shared.Request.Type.*;

public class Request {


    public enum Type {
        HI, POSITION, RUN_OUT_FUEL, NEW_DESTINATION, DESTINATION, FREEZE, THAW;


    }
    public final String aircraftName;
    public final Position position;
    public final Type type;
    public final boolean noMoreDestination;

    public final Fuel fuel;
    private Request(String aircraftName, Position position, Type type, boolean noMoreDestination, Fuel fuel) {
        this.aircraftName = aircraftName;
        this.position = position;
        this.type = type;
        this.noMoreDestination = noMoreDestination;
        this.fuel = fuel;
    }
    public static Request hi(String aircraftName) {
        return new Request(aircraftName, null,  HI, false, null);
    }
    public static Request position(String aircraftName, Position position, boolean noMoreDestination, Fuel fuel ) {
        return new Request(aircraftName, position,  POSITION, noMoreDestination, fuel);
    }
    public static Request runOutOfFuel(String aircraftName) {
        return new Request(aircraftName, null,  RUN_OUT_FUEL, false, null);
    }

    public static Request newDirection(String aircraftName) {
        return new Request(aircraftName, null, NEW_DESTINATION, false, null);
    }
    public static Request direction(String planeName) {
        return new Request(planeName, null, DESTINATION, false, null);
    }

    public boolean isPosition() {
        return type == POSITION;
    }

    public boolean isRunOutOfFuel() {
        return type == RUN_OUT_FUEL;
    }

    public boolean isNewDestination() {
        return type == NEW_DESTINATION;
    }

    public boolean isDestination() {
        return type == DESTINATION;
    }

    public static Request freeze(String planeName) {
        return new Request(planeName, null, FREEZE, false, null);
    }

    public boolean isFreeze() {
        return type == FREEZE;
    }

    public static Request thaw(String planeName) {
        return new Request(planeName, null, THAW, false, null);
    }

    public boolean isThaw() {
        return type == THAW;
    }
}
