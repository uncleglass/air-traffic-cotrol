package pl.uncleglass.airtrafficcontrol.shared;

import java.util.Objects;

import static pl.uncleglass.airtrafficcontrol.Property.MIN_LATITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_LATITUDE;

public final class Latitude {
    public final int value;

    private Latitude(int value) {
        if (value < MIN_LATITUDE || value > MAX_LATITUDE) {
            throw new IllegalArgumentException("Value of latitude is out of the range");
        }
        this.value = value;
    }

    public static Latitude of(int value) {
        return new Latitude(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Latitude latitude = (Latitude) o;
        return value == latitude.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public Latitude makeCloser(Latitude destination) {
        if (equals(destination)) {
            return this;
        }
        if (value < destination.value) {
            return of(value + 1);
        } else {
            return of(value - 1);
        }
    }

    public Latitude add(int i) {
        return of(value + i);
    }

    public Latitude subtract(int i) {
        return of(value - i);
    }

    public boolean isTooClose(Latitude latitude) {
        return Math.abs(value - latitude.value) < 2;
    }

    @Override
    public String toString() {
        return "Latitude{" +
                "value=" + value +
                '}';
    }
}
