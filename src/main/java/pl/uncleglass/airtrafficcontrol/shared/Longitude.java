package pl.uncleglass.airtrafficcontrol.shared;

import java.util.Objects;

import static pl.uncleglass.airtrafficcontrol.Property.MIN_LONGITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_LONGITUDE;

public final class Longitude {
    public final int value;

    private Longitude(int value) {
        if (value < MIN_LONGITUDE || value > MAX_LONGITUDE) {
            throw new IllegalArgumentException("Value of longitude is out of the range");
        }
        this.value = value;
    }

    public static Longitude of(int value) {
        return new Longitude(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Longitude longitude = (Longitude) o;
        return value == longitude.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public Longitude makeCloser(Longitude destination) {
        if (equals(destination)) {
            return this;
        }
        if (value < destination.value) {
            return of(value + 1);
        } else {
            return of(value - 1);
        }
    }

    public Longitude add(int i) {
        return of(value + i);
    }

    public Longitude subtract(int i) {
        return of(value - i);
    }

    public boolean isTooClose(Longitude longitude) {
        return Math.abs(value - longitude.value) < 2;
    }

    @Override
    public String toString() {
        return "Longitude{" +
                "value=" + value +
                '}';
    }
}