package pl.uncleglass.airtrafficcontrol.shared;

import java.util.Objects;

import static pl.uncleglass.airtrafficcontrol.Property.MIN_HEIGHT;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_HEIGHT;

public final class Altitude {
    public final double value;

    private Altitude(double value) {
        if (value < MIN_HEIGHT || value > MAX_HEIGHT) {
            throw new IllegalArgumentException("Value of altitude is out of the range");
        }
        this.value = value;
    }

    public static Altitude of(double value) {
        return new Altitude(value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Altitude altitude = (Altitude) o;
        return value == altitude.value;
    }

    @Override
    public int hashCode() {
        return Objects.hash(value);
    }

    public Altitude makeCloser(Altitude destination) {
        if (equals(destination)) {
            return this;
        }
        if (value < destination.value) {
            return of(value + 0.2);
        } else {
            double v = value - 0.2;
            return of(v < 0 ? 0 : v);
        }
    }

    public Altitude add(int i) {
        return of(value + i);
    }

    public boolean isTooClose(Altitude altitude) {
        return Math.abs(value - altitude.value) < 2;
    }

    @Override
    public String toString() {
        return "Altitude{" +
                "value=" + value +
                '}';
    }
}