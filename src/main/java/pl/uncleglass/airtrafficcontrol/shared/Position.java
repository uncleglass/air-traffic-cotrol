package pl.uncleglass.airtrafficcontrol.shared;

import java.util.Objects;

public final class Position {
    public final Latitude latitude;
    public final Longitude longitude;
    public final Altitude altitude;

    private Position(Latitude latitude, Longitude longitude, Altitude altitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.altitude = altitude;
    }

    public static Position of(int latitude, int longitude, double height) {
        return new Position(
                Latitude.of(latitude),
                Longitude.of(longitude),
                Altitude.of(height)
        );
    }

    public static Position of(Latitude latitude, Longitude longitude, Altitude altitude) {
        return new Position(latitude, longitude, altitude);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Position position = (Position) o;
        return Objects.equals(latitude, position.latitude)
                && Objects.equals(longitude, position.longitude)
                && Objects.equals(altitude, position.altitude);
    }

    @Override
    public int hashCode() {
        return Objects.hash(latitude, longitude, altitude);
    }

    public Position makeCloser(Position destination) {
        return new Position(
                latitude.makeCloser(destination.latitude),
                longitude.makeCloser(destination.longitude),
                altitude.makeCloser(destination.altitude)
        );
    }

    public boolean isTooClose(Position position) {
        if (position == null) {
            return false;
        }
        return latitude.isTooClose(position.latitude)
                && longitude.isTooClose(position.longitude)
                && altitude.isTooClose(position.altitude);
    }

    public boolean reached(Position position) {
        return latitude.equals(position.latitude)
                && longitude.equals(position.longitude);
    }

    @Override
    public String toString() {
        return "Position{" + latitude +
                ", " + longitude +
                ", " + altitude +
                '}';
    }
}
