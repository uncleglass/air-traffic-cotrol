package pl.uncleglass.airtrafficcontrol.shared;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonConverter {
    private JsonConverter() {
    }

    public static String to(Object object) {
        Gson gson = new GsonBuilder().serializeNulls().create();
        return gson.toJson(object);
    }

    public static <T> T from(String message, Class<T> classType) {
        return new Gson().fromJson(message, classType);
    }
}
