package pl.uncleglass.airtrafficcontrol.shared;

public enum FlyDirection {
    LAT_UP, LAT_DOWN, LON_UP, LON_DOWN;

    public static FlyDirection check(Position actualPosition, Position lastPosition) {
        Latitude actualLatitude = actualPosition.latitude;
        Longitude actualLongitude = actualPosition.longitude;
        Latitude lastLatitude = lastPosition.latitude;
        Longitude lastLongitude = lastPosition.longitude;
        if (actualLatitude.value > lastLatitude.value) {
            return LAT_UP;
        }
        if (actualLatitude.value < lastLatitude.value) {
            return LAT_DOWN;
        }
        if (actualLongitude.value > lastLongitude.value) {
            return LON_UP;
        }
        if (actualLongitude.value < lastLongitude.value) {
            return LON_DOWN;
        }
        throw new IllegalStateException();
    }
}
