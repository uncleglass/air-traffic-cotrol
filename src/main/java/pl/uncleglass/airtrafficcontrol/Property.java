package pl.uncleglass.airtrafficcontrol;

public class Property {
    private Property() {
    }

    public static final int AIRPORT_DIMENSIONS_IN_METERS = 10_000;
    public static final int AIRPORT_HEIGHT_IN_METERS = 5000;
    public static final int SECTOR_IN_METERS = 10;
    public static final int MIN_LONGITUDE = 0;
    public static final int MAX_LONGITUDE = AIRPORT_DIMENSIONS_IN_METERS / SECTOR_IN_METERS;
    public static final int MIN_LATITUDE = 0;
    public static final int MAX_LATITUDE = AIRPORT_DIMENSIONS_IN_METERS / SECTOR_IN_METERS;
    public static final int MIN_HEIGHT = 0;
    public static final int MIN_APPEARANCE_HEIGHT = 2000 / SECTOR_IN_METERS;
    public static final int MAX_HEIGHT = AIRPORT_HEIGHT_IN_METERS / SECTOR_IN_METERS;
    public static final String SERVER_ADDRESS = "127.0.0.1";
    public static final int SERVER_PORT = 9090;
    public static final long TIME_UNIT = 100;
    public static final int AIRCRAFT_COUNT_LIMIT = 100;
    public static final int FUEL_QUANTITY = 10800;
    public static final int SPEED_IN_METERS_PER_SEC = 50;
    public static final int TURN_POINT = 500 / SECTOR_IN_METERS;
}
