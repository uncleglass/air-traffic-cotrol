package pl.uncleglass.airtrafficcontrol.groundcontrol;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.airtrafficcontrol.Property;
import pl.uncleglass.airtrafficcontrol.aircraft.Fuel;
import pl.uncleglass.airtrafficcontrol.db.dao.CrashDAO;
import pl.uncleglass.airtrafficcontrol.db.dto.CrashDTO;
import pl.uncleglass.airtrafficcontrol.shared.Altitude;
import pl.uncleglass.airtrafficcontrol.shared.ConnectionHandler;
import pl.uncleglass.airtrafficcontrol.shared.FlyDirection;
import pl.uncleglass.airtrafficcontrol.shared.JsonConverter;
import pl.uncleglass.airtrafficcontrol.shared.Position;
import pl.uncleglass.airtrafficcontrol.shared.Request;
import pl.uncleglass.airtrafficcontrol.shared.Response;

import java.time.LocalDateTime;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class AircraftHandler implements Runnable {
    private static final Logger logger = LogManager.getLogger(AircraftHandler.class);
    private final ConnectionHandler connection;
    private final PlaneInfo planeInfo;
    private final Trajectory trajectory = new Trajectory();
    private boolean run = true;
    private boolean workInProgress = false;
    private boolean noMoreDestination;
    private Fuel fuel;

    public AircraftHandler(ConnectionHandler connection, PlaneInfo aircraftName) {
        this.connection = connection;
        this.planeInfo = aircraftName;
    }

    @Override
    public void run() {
        while (run) {
            try {
                TimeUnit.MILLISECONDS.sleep(Property.TIME_UNIT / 4);
            } catch (InterruptedException e) {
//                e.printStackTrace();
                //TODO log it
            }
        }

        System.out.println("aircrafthandler sttoped");
    }

    public void close() {
        connection.close();
        run = false;
    }

    public void askForPosition() {
        String response = JsonConverter.to(Response.position());
        connection.send(response);

        Request request = JsonConverter.from(connection.receive(), Request.class);
        if (request.isRunOutOfFuel()) {
            logger.info("{} - run out of fuel", planeInfo.name);
            CrashDTO crashToSave = CrashDTO.of(planeInfo.id, getCurrentPosition().toString(), LocalDateTime.now());
            CrashDAO.saveCrash(crashToSave);
            String runOutOfFuel = JsonConverter.to(Response.runOutOfFuel());
            connection.send(runOutOfFuel);
            close();
            return;
        }
        synchronized (this) {
            trajectory.add(request.position);
        }
        noMoreDestination = request.noMoreDestination;
        fuel = request.fuel;
    }

    public boolean isNotRun() {
        return !run;
    }

    public String getAircraftName() {
        return planeInfo.name;
    }

    public void sendNewDestination(List<Position> destination) {
        workInProgress = true;
        String response = JsonConverter.to(Response.newDestination(destination));
        connection.send(response);

        Request request = JsonConverter.from(connection.receive(), Request.class);
        if (request.isNewDestination()) {
//            System.out.println("New destination for " + request.aircraftName + " was applied");
        } else {
            System.out.println("Something went wrong");
        }
    }

    public void sendDestination(List<Position> positions) {
        String response = JsonConverter.to(Response.destination(positions));
        connection.send(response);

        Request request = JsonConverter.from(connection.receive(), Request.class);
        if (request.isDestination()) {
//            System.out.println("Destination for " + request.aircraftName + " was applied");
        } else {
            System.out.println("Something went wrong");
        }
    }

    public Position getCurrentPosition() {
        return trajectory.getLatest();
    }

    public FlyDirection getFlyDirection() {
        return trajectory.getFlyDirection();
    }

    public boolean enoughDataToWork() {
        return trajectory.size() > 2;
    }

    public boolean isNoWorkInProgress() {
        return !workInProgress;
    }

    public boolean isNoMoreDestination() {
        return noMoreDestination;
    }

    public Fuel getFuel() {
        return fuel;
    }

    public boolean isLanded() {
        return trajectory.getLatest().altitude.equals(Altitude.of(0));
    }

    public void hasLanded() {
        String response = JsonConverter.to(Response.hasLanded());
        connection.send(response);
        close();
    }

    public boolean isTooClose(AircraftHandler aircraftHandler) {
        if (aircraftHandler == null) {
            return false;
        }
        Position currentPosition = aircraftHandler.getCurrentPosition();
        if (currentPosition == null) {
            return false;
        }
        Position position = getCurrentPosition();
        if (position == null) {
            return false;
        }
        return position.isTooClose(currentPosition);
    }

    public void collision() {
        String response = JsonConverter.to(Response.collision());
        connection.send(response);
        close();
    }

    public PlaneInfo getPlaneInfo() {
        return planeInfo;
    }

    public void freeze() {
        String response = JsonConverter.to(Response.freeze());
        connection.send(response);

        Request request = JsonConverter.from(connection.receive(), Request.class);
        if (request.isFreeze()) {
            logger.info("{} is frozen", planeInfo.name);
        } else {
            logger.warn("Freezing for {} went wrong", planeInfo.name);
        }
    }

    public void thaw() {
        String response = JsonConverter.to(Response.thaw());
        connection.send(response);

        Request request = JsonConverter.from(connection.receive(), Request.class);
        if (request.isThaw()) {
            logger.info("{} is thaw", planeInfo.name);
        } else {
            logger.warn("Thawing for {} went wrong", planeInfo.name);
        }
    }

    public void stop() {
        String response = JsonConverter.to(Response.stop());
        connection.send(response);
        close();
    }
}
