package pl.uncleglass.airtrafficcontrol.groundcontrol;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import pl.uncleglass.airtrafficcontrol.Property;
import pl.uncleglass.airtrafficcontrol.aircraft.Fuel;
import pl.uncleglass.airtrafficcontrol.db.dao.CrashDAO;
import pl.uncleglass.airtrafficcontrol.db.dao.LandingDAO;
import pl.uncleglass.airtrafficcontrol.db.dto.CrashDTO;
import pl.uncleglass.airtrafficcontrol.db.dto.LandingDTO;
import pl.uncleglass.airtrafficcontrol.shared.FlyDirection;
import pl.uncleglass.airtrafficcontrol.shared.Altitude;
import pl.uncleglass.airtrafficcontrol.shared.Latitude;
import pl.uncleglass.airtrafficcontrol.shared.Longitude;
import pl.uncleglass.airtrafficcontrol.shared.Position;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public class Controller implements Runnable {
    private static final Logger logger = LogManager.getLogger(Controller.class);
    private final List<AircraftHandler> aircraftHandlers;
    //    private final List<AircraftHandler> landing = new ArrayList<>();
    private final List<AircraftHandler> landed = new ArrayList<>();
    private int counter = 0;
    private boolean frozen;
    private boolean run = true;

    public Controller(List<AircraftHandler> aircraftHandlers) {
        this.aircraftHandlers = aircraftHandlers;
    }

    @Override
    public void run() {
        System.out.println("Controller has started.");
        int counter = 0;
        while (run) {
            counter++;
            if (!aircraftHandlers.isEmpty()) {
                cleanUp();
                if (frozen) {
                    continue;
                }
                askAllForPosition();
                findConflicts();
                cleanUp();
                if (counter % 2 == 0) {
                    cleanUp();
                    findPlanesInOuterZone();
                }
                if (counter % 4 == 0) {
                    cleanUp();
                    findPlanesWithoutDestination();
                }
                if (counter % 8 == 0) {
                    cleanUp();
                    corridors();
                }
            }

            try {
                TimeUnit.MILLISECONDS.sleep(Property.TIME_UNIT / 4);
            } catch (InterruptedException e) {
//                e.printStackTrace();
                //TODO log it
            }
        }

        System.out.println("Controller has ended.");

    }

    private void findConflicts() {
        synchronized (aircraftHandlers) {

            for (int i = 0, k = aircraftHandlers.size(); i < k - 1; i++) {
                for (int j = i + 1; j < k; j++) {
                    AircraftHandler handler1 = aircraftHandlers.get(i);
                    AircraftHandler handler2 = aircraftHandlers.get(j);
                    if (handler1 != handler2 && handler1.isTooClose(handler2)) {
                        PlaneInfo planeOneInfo = handler1.getPlaneInfo();
                        PlaneInfo planeTwoInfo = handler2.getPlaneInfo();
                        logger.info("There was a conflict between {} and {}", planeOneInfo.name, planeTwoInfo.name);
                        CrashDTO crashToSave = CrashDTO.of(
                                planeOneInfo.id,
                                planeTwoInfo.id,
                                handler1.getCurrentPosition().toString(),
                                handler2.getCurrentPosition().toString(),
                                LocalDateTime.now()
                        );
                        CrashDAO.saveCollision(crashToSave);
                        handler1.collision();
                        handler2.collision();
                    }
                }
            }
        }
    }

    private void cleanUp() {
        synchronized (aircraftHandlers) {
            List<AircraftHandler> collect = aircraftHandlers.stream()
                    .filter(AircraftHandler::isNotRun)
                    .collect(Collectors.toList());
            if (!collect.isEmpty()) {
                aircraftHandlers.removeAll(collect);
            }
        }
//        List<AircraftHandler> collect = landing.stream()
//                .filter(AircraftHandler::isNotRun)
//                .collect(Collectors.toList());
//        landing.removeAll(collect);

        List<AircraftHandler> collect1 = landed.stream()
                .filter(AircraftHandler::isNotRun)
                .collect(Collectors.toList());
        landed.removeAll(collect1);

    }

    private void askAllForPosition() {
        synchronized (aircraftHandlers) {
            aircraftHandlers.forEach(AircraftHandler::askForPosition);
        }
    }

    private void corridors() {
//        moveFromInterceptorToWaiting();
//        if (landing.size() < 2) {
//            findForLanding();
//        }
        if (!landed.isEmpty()) {
            checkIfLanded();
        }
    }

    private void checkIfLanded() {
        List<AircraftHandler> list;

        synchronized (landed) {
            list = landed.stream()
                    .filter(AircraftHandler::isLanded)
                    .collect(Collectors.toList());
        }

        if (!list.isEmpty()) {
            list.forEach(aircraftHandler -> {
                PlaneInfo planeInfo = aircraftHandler.getPlaneInfo();
                LandingDTO landingToSave = LandingDTO.of(planeInfo.id, LocalDateTime.now());
                LandingDAO.save(landingToSave);
                logger.info("Plane {} has landed ", aircraftHandler.getAircraftName());
            });
            landed.removeAll(list);
            list.forEach(AircraftHandler::hasLanded);
        }
    }


//    private void findForLanding() {
//        List<AircraftHandler> planes;
//
//        synchronized (aircraftHandlers) {
//            planes = aircraftHandlers.stream()
//                    .filter(AreaAnalyser::isInWaiting)
//                    .collect(Collectors.toList());
//        }
//
//        AircraftHandler handler = findWithTheLeastAmountOfFuel(planes);
//
//        if (handler != null) {
//            landing.add(handler);
//            Position currentPosition = handler.getCurrentPosition();
//            FlyDirection flyDirection = handler.getFlyDirection();
//            List<Position> positionForLandingCorridor = findPositionForLandingCorridor(currentPosition, flyDirection);
//            handler.sendNewDestination(positionForLandingCorridor);
//        }
//    }
    private AircraftHandler findWithTheLeastAmountOfFuel(List<AircraftHandler> planes) {
        AircraftHandler plane = null;
        Fuel fuel = Fuel.of(Property.FUEL_QUANTITY);
        for (AircraftHandler handler : planes) {
            Fuel f = handler.getFuel();
            if (f.value < fuel.value) {
                fuel = f;
                plane = handler;
            }
        }
        return plane;
    }

    private boolean isFreeSpaceAtWaitingCorridor(AircraftHandler aircraftHandler) {
        Position currentPosition = aircraftHandler.getCurrentPosition();
        Latitude latitude = currentPosition.latitude;
        Longitude longitude = currentPosition.longitude;
        Altitude altitude = currentPosition.altitude;

        FlyDirection flyDirection = aircraftHandler.getFlyDirection();
        List<AircraftHandler> collect;

        synchronized (aircraftHandlers) {
            collect = aircraftHandlers.stream()
                    .filter(AircraftHandler::enoughDataToWork)
                    .filter(ah -> flyDirection == ah.getFlyDirection())
                    .filter(ah -> ah.getCurrentPosition().altitude.equals(altitude))
                    .filter(ah -> {
                                FlyDirection fd = ah.getFlyDirection();
                                Position p = ah.getCurrentPosition();
                                int lon = p.longitude.value;
                                int lat = p.latitude.value;
                                switch (fd) {
                                    case LON_UP:
                                    case LON_DOWN:
                                        int v = longitude.value;
                                        return lon < v + 25 && lon > v - 25
                                                && (lat == 200 || lat == 800);
                                    case LAT_UP:
                                    case LAT_DOWN:
                                        int l = latitude.value;
                                        return lat < l + 25 && lat > l - 25
                                                && (lon == 200 || lon == 800);
                                }
                                return false;
                            }
                    )
                    .collect(Collectors.toList());
        }


        return collect.isEmpty();
    }

    private void findPlanesInOuterZone() {
        List<AircraftHandler> planes;

        synchronized (aircraftHandlers) {
            planes = aircraftHandlers.stream()
                    .filter(AircraftHandler::isNoWorkInProgress)
                    .filter(AircraftHandler::enoughDataToWork)
                    .filter(AreaAnalyser::isInOuterZone)
                    .collect(Collectors.toList());
        }

        planes.forEach(this::sendToInterceptorCorridor);
    }

    private void sendToInterceptorCorridor(AircraftHandler aircraftHandler) {
        Position currentPosition = aircraftHandler.getCurrentPosition();
        FlyDirection flyDirection = aircraftHandler.getFlyDirection();
        List<Position> destination = findPositionForInterceptorCorridor(currentPosition, flyDirection);
        aircraftHandler.sendNewDestination(destination);
    }

    private void sendToWaitingCorridor(AircraftHandler aircraftHandler) {
        Position currentPosition = aircraftHandler.getCurrentPosition();
        List<Position> destination = findPositionForWaitingCorridor(currentPosition);
        aircraftHandler.sendNewDestination(destination);
    }

    private List<Position> findPositionForInterceptorCorridor(Position actualPosition, FlyDirection flyDirection) {
        AreaAnalyser.Zone zone = AreaAnalyser.checkOuterZone(actualPosition, flyDirection);
        Altitude altitude = AreaAnalyser.checkCorridorHeight(actualPosition);
        List<Position> positions = new ArrayList<>();
        switch (zone) {
            case LEFT_BIG:
            case TOP_SMALL:
                positions.add(Position.of(50, 950, 450)); //1
                positions.add(Position.of(950, 950, 450)); //2
                positions.add(Position.of(950, 50, 450)); //3
                positions.add(Position.of(100, 50, 450)); //4
                return positions;
            case LEFT_SMALL:
            case BOTTOM_BIG:
                positions.add(Position.of(950, 950, 450)); //2
                positions.add(Position.of(950, 50, 450)); //3
                positions.add(Position.of(100, 50, 450)); //4
                return positions;
            case BOTTOM_SMALL:
            case RIGHT_BIG:
                positions.add(Position.of(950, 50, 450)); //3
                positions.add(Position.of(100, 50, 450)); //4
                return positions;
            case RIGHT_SMALL:
            case TOP_BIG:
                positions.add(Position.of(50, 50, 450)); //4
                positions.add(Position.of(50, 950, 450)); //1
                positions.add(Position.of(950, 950, 450)); //2
                positions.add(Position.of(950, 50, 450)); //3
                positions.add(Position.of(100, 50, 450)); //4
                return positions;
        }
        throw new IllegalStateException();
    }

    private List<Position> findPositionForWaitingCorridor(Position currentPosition) {
        Altitude altitude = AreaAnalyser.checkCorridorHeight(currentPosition);
        List<Position> positions = new ArrayList<>();
        Latitude latitude = currentPosition.latitude;
        Longitude longitude = currentPosition.longitude;
        if (latitude.value == 100 && longitude.value > 99 && longitude.value < 901) {
            if (longitude.value < 800) {
                positions.add(Position.of(200, 800, altitude.value)); //1
                positions.add(Position.of(800, 800, altitude.value)); //2
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            } else {
                positions.add(Position.of(800, 800, altitude.value)); //2
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            }
            return positions;
        }
        if (latitude.value > 99 && latitude.value < 901 && longitude.value == 900) {
            if (latitude.value < 800) {
                positions.add(Position.of(800, 800, altitude.value)); //2
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            } else {
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            }
            return positions;
        }
        if (latitude.value == 900 && longitude.value > 99 && longitude.value < 901) {
            if (longitude.value > 200) {
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            } else {
                positions.add(Position.of(200, 200, altitude.value)); //4
                positions.add(Position.of(200, 800, altitude.value)); //1
                positions.add(Position.of(800, 800, altitude.value)); //2
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            }
            return positions;
        }
        if (latitude.value > 99 && latitude.value < 901 && longitude.value == 100) {
            if (latitude.value > 200) {
                positions.add(Position.of(200, 200, altitude.value)); //4
                positions.add(Position.of(200, 800, altitude.value)); //1
                positions.add(Position.of(800, 800, altitude.value)); //2
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            } else {
                positions.add(Position.of(200, 800, altitude.value)); //1
                positions.add(Position.of(800, 800, altitude.value)); //2
                positions.add(Position.of(800, 200, altitude.value)); //3
                positions.add(Position.of(200, 200, altitude.value)); //4
            }
            return positions;
        }
        throw new IllegalStateException();
    }

    private List<Position> findPositionForLandingCorridor(Position currentPosition, FlyDirection flyDirection) {
        AreaAnalyser.Zone zone = AreaAnalyser.checkWaitingZone(currentPosition, flyDirection);
        Altitude altitude = AreaAnalyser.checkCorridorHeight(currentPosition);
        List<Position> positions = new ArrayList<>();
        switch (zone) {
            case LEFT_BIG:
            case TOP_SMALL:
                positions.add(Position.of(300, 700, altitude.value)); //1
                positions.add(Position.of(700, 700, altitude.value)); //2
                positions.add(Position.of(700, 300, altitude.value)); //3
                positions.add(Position.of(300, 300, altitude.value)); //4

                return positions;
            case LEFT_SMALL:
            case BOTTOM_BIG:
                positions.add(Position.of(700, 700, altitude.value)); //2
                positions.add(Position.of(700, 300, altitude.value)); //3
                positions.add(Position.of(300, 300, altitude.value)); //4

                return positions;
            case BOTTOM_SMALL:
            case RIGHT_BIG:
                positions.add(Position.of(700, 300, altitude.value)); //3
                positions.add(Position.of(300, 300, altitude.value)); //4

                return positions;
            case RIGHT_SMALL:
            case TOP_BIG:
                positions.add(Position.of(300, 300, altitude.value)); //4
                positions.add(Position.of(300, 700, altitude.value)); //1
                positions.add(Position.of(700, 700, altitude.value)); //2
                positions.add(Position.of(700, 300, altitude.value)); //3
                positions.add(Position.of(300, 300, altitude.value)); //4

                return positions;
        }
        throw new IllegalStateException();
    }

    private List<Position> getCorridorCoordinates(AircraftHandler aircraftHandler) {
        Altitude altitude = aircraftHandler.getCurrentPosition().altitude;
        Longitude longitude = aircraftHandler.getCurrentPosition().longitude;
        List<Position> positions = new ArrayList<>();
        if (longitude.equals(Longitude.of(300))) {

            if (counter % 2 == 0) {
                positions.add(Position.of(300, 350, 40));
                positions.add(Position.of(300, 380, 30));
                positions.add(Position.of(350, 400, 25));
                positions.add(Position.of(400, 400, 20));
                positions.add(Position.of(450, 400, 15));
                positions.add(Position.of(500, 400, 10));
                positions.add(Position.of(550, 400, 5));
                positions.add(Position.of(600, 400, 0));
                positions.add(Position.of(650, 400, 0));
                positions.add(Position.of(700, 400, 0));
                positions.add(Position.of(750, 400, 0));
                positions.add(Position.of(800, 400, 0));
            } else {
                positions.add(Position.of(300, 350, 40));
                positions.add(Position.of(300, 580, 30));
                positions.add(Position.of(350, 600, 25));
                positions.add(Position.of(400, 600, 20));
                positions.add(Position.of(450, 600, 15));
                positions.add(Position.of(500, 600, 10));
                positions.add(Position.of(550, 600, 5));
                positions.add(Position.of(600, 600, 0));
                positions.add(Position.of(650, 600, 0));
                positions.add(Position.of(700, 600, 0));
                positions.add(Position.of(750, 600, 0));
                positions.add(Position.of(800, 600, 0));
            }
            counter++;
//                landing.remove(aircraftHandler);
            landed.add(aircraftHandler);


        } else {
            positions.add(Position.of(100, 900, 400)); //1
            positions.add(Position.of(900, 900, 350)); //2
            positions.add(Position.of(900, 100, 300)); //3
            positions.add(Position.of(200, 100, 250)); //4
            positions.add(Position.of(200, 800, 200)); //1
            positions.add(Position.of(800, 800, 150)); //2
            positions.add(Position.of(800, 300, 100)); //3
            positions.add(Position.of(300, 300, 50)); //4
        }
        return positions;
    }

    private void findPlanesWithoutDestination() {
        List<AircraftHandler> planes;

        synchronized (aircraftHandlers) {
            planes = aircraftHandlers.stream()
                    .filter(AircraftHandler::isNoMoreDestination)
//                    .filter(AreaAnalyser::isOutOuterZone)
                    .collect(Collectors.toList());
        }

        planes.forEach(aircraftHandler -> {
            List<Position> positions = getCorridorCoordinates(aircraftHandler);
            aircraftHandler.sendDestination(positions);
        });
    }

    public void freeze() {
        frozen = true;
        synchronized (aircraftHandlers) {
            aircraftHandlers.forEach(AircraftHandler::freeze);
        }
    }

    public void thaw() {
        frozen = false;
        synchronized (aircraftHandlers) {
            aircraftHandlers.forEach(AircraftHandler::thaw);
        }
    }

    public void stop() {
        synchronized (aircraftHandlers) {
            aircraftHandlers.forEach(AircraftHandler::stop);
        }
        run = false;
    }
}
