package pl.uncleglass.airtrafficcontrol.groundcontrol;

import pl.uncleglass.airtrafficcontrol.shared.FlyDirection;
import pl.uncleglass.airtrafficcontrol.shared.Position;

import java.util.ArrayList;
import java.util.List;

public class Trajectory {
    private final List<Position> values = new ArrayList<>();

    public void add(Position position) {
        if (values.isEmpty()) {
            values.add(position);
        } else {
            Position latest = getLatest();
            if (!latest.equals(position)) {
                values.add(position);
            }
        }
    }

    public Position getLatest() {
        if (values.isEmpty()) {
            return null;
        }
        return values.get(lastIndex());
    }

    private int lastIndex() {
        return values.size() - 1;
    }

    public FlyDirection getFlyDirection() {
        return FlyDirection.check(getLatest(), getNextToLast());
    }

    private Position getNextToLast() {
        return values.get(nextToLastIndex());
    }

    private int nextToLastIndex() {
        return values.size() - 2;
    }

    public int size() {
        return values.size();
    }

    public boolean contains(Position position) {
        return values.contains(position);
    }

}
