package pl.uncleglass.airtrafficcontrol.groundcontrol;

import pl.uncleglass.airtrafficcontrol.shared.FlyDirection;
import pl.uncleglass.airtrafficcontrol.shared.Altitude;
import pl.uncleglass.airtrafficcontrol.shared.Latitude;
import pl.uncleglass.airtrafficcontrol.shared.Longitude;
import pl.uncleglass.airtrafficcontrol.shared.Position;

import static pl.uncleglass.airtrafficcontrol.Property.MAX_LATITUDE;
import static pl.uncleglass.airtrafficcontrol.Property.MAX_LONGITUDE;

public class AreaAnalyser {


    public enum Zone {
        LEFT_BIG, LEFT_SMALL, BOTTOM_BIG, BOTTOM_SMALL, RIGHT_BIG, RIGHT_SMALL, TOP_BIG, TOP_SMALL;
    }

    public static boolean isInOuterZone(AircraftHandler aircraftHandler) {
        Position actualPosition = aircraftHandler.getCurrentPosition();
        Latitude latitude = actualPosition.latitude;
        Longitude longitude = actualPosition.longitude;

        return latitude.value < MAX_LATITUDE && longitude.value < 100
                || latitude.value < 100 && longitude.value < MAX_LONGITUDE
                || latitude.value < MAX_LATITUDE && longitude.value > 900
                || latitude.value > 900 && longitude.value < MAX_LONGITUDE;
    }

    public static boolean isOutOuterZone(AircraftHandler aircraftHandler) {
        return !isInOuterZone(aircraftHandler);
    }


    public static Zone checkOuterZone(Position actualPosition, FlyDirection flyDirection) {
        Latitude latitude = actualPosition.latitude;
        Longitude longitude = actualPosition.longitude;
        switch (flyDirection) {
            case LAT_UP:
                if (latitude.value < 100 && longitude.value < 900) {
                    return Zone.LEFT_BIG;
                } else {
                    return Zone.LEFT_SMALL;
                }
            case LON_DOWN:
                if (latitude.value < 900 && longitude.value > 900) {
                    return Zone.BOTTOM_BIG;
                } else {
                    return Zone.BOTTOM_SMALL;
                }
            case LAT_DOWN:
                if (latitude.value > 900 && longitude.value > 100) {
                    return Zone.RIGHT_BIG;
                } else {
                    return Zone.RIGHT_SMALL;
                }
            case LON_UP:
                if (latitude.value > 100 && longitude.value < 100) {
                    return Zone.TOP_BIG;
                } else {
                    return Zone.TOP_SMALL;
                }
        }
        throw new IllegalStateException();
    }

    public static Zone checkWaitingZone(Position currentPosition, FlyDirection flyDirection) {
        Latitude latitude = currentPosition.latitude;
        Longitude longitude = currentPosition.longitude;
        switch (flyDirection) {
            case LON_UP:
                if (longitude.value > 199 && longitude.value < 801) {
                    return Zone.LEFT_BIG;
                } else {
                    return Zone.LEFT_SMALL;
                }
            case LAT_UP:
                if (latitude.value > 199 && latitude.value < 801) {
                    return Zone.BOTTOM_BIG;
                } else {
                    return Zone.BOTTOM_SMALL;
                }
            case LON_DOWN:
                if (longitude.value < 801 && longitude.value > 300) {
                    return Zone.RIGHT_BIG;
                } else {
                    return Zone.RIGHT_SMALL;
                }
            case LAT_DOWN:
                if (latitude.value > 300 && latitude.value < 801) {
                    return Zone.TOP_BIG;
                } else {
                    return Zone.TOP_SMALL;
                }
        }
        throw new IllegalStateException();
    }

    public static boolean isInInterceptor(AircraftHandler aircraftHandler) {
        Position actualPosition = aircraftHandler.getCurrentPosition();
        if (actualPosition == null) {
            return false;
        }
        Latitude latitude = actualPosition.latitude;
        Longitude longitude = actualPosition.longitude;

        return latitude.value == 100 && longitude.value > 100 && longitude.value < 550
                || latitude.value > 100 && latitude.value < 550 && longitude.value == 900
                || latitude.value == 900 && longitude.value > 450 && longitude.value < 900
                || latitude.value > 450 && latitude.value < 900 && longitude.value == 100;

    }

    public static boolean isInWaiting(AircraftHandler aircraftHandler) {
        Position actualPosition = aircraftHandler.getCurrentPosition();
        if (actualPosition == null) {
            return false;
        }
        Latitude latitude = actualPosition.latitude;
        Longitude longitude = actualPosition.longitude;

        return latitude.value == 200 && longitude.value > 199 && longitude.value < 801
                || latitude.value > 199 && latitude.value < 801 && longitude.value == 800
                || latitude.value == 800 && longitude.value > 199 && longitude.value < 801
                || latitude.value > 199 && latitude.value < 801 && longitude.value == 200;
    }

    public static Altitude checkCorridorHeight(Position position) {
        double value = position.altitude.value;
        if (value < 225) {
            return Altitude.of(210);
        }
        if (value < 255) {
            return Altitude.of(240);
        }
        if (value < 285) {
            return Altitude.of(270);
        }
        if (value < 315) {
            return Altitude.of(300);
        }
        if (value < 345) {
            return Altitude.of(330);
        }
        if (value < 375) {
            return Altitude.of(360);
        }
        if (value < 405) {
            return Altitude.of(390);
        }
        if (value < 435) {
            return Altitude.of(420);
        }
        if (value < 465) {
            return Altitude.of(450);
        }
        return Altitude.of(480);
    }
}
