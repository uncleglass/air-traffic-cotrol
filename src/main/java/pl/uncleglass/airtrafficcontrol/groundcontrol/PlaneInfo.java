package pl.uncleglass.airtrafficcontrol.groundcontrol;

public class PlaneInfo {
    public final Long id;
    public final String name;

    public PlaneInfo(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public String toString() {
        return "PlaneInfo{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
