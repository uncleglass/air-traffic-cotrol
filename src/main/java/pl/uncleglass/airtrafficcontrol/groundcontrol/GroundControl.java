package pl.uncleglass.airtrafficcontrol.groundcontrol;

import pl.uncleglass.airtrafficcontrol.Property;
import pl.uncleglass.airtrafficcontrol.db.dao.PlaneDAO;
import pl.uncleglass.airtrafficcontrol.db.dto.PlaneDTO;
import pl.uncleglass.airtrafficcontrol.shared.ConnectionHandler;
import pl.uncleglass.airtrafficcontrol.shared.JsonConverter;
import pl.uncleglass.airtrafficcontrol.shared.Request;
import pl.uncleglass.airtrafficcontrol.shared.Response;

import java.io.IOException;
import java.net.ServerSocket;
import java.time.LocalDateTime;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static pl.uncleglass.airtrafficcontrol.Property.SERVER_PORT;

public class GroundControl implements Runnable {
    private final List<AircraftHandler> aircraftHandlers = Collections.synchronizedList(new LinkedList<>());
    private final ExecutorService executorService = Executors.newFixedThreadPool(102, r -> new Thread(r, "ground-control"));
    private Controller controller;
    private boolean run;
    ServerSocket listener;

    @Override
    public void run() {
        run = true;

        controller = new Controller(aircraftHandlers);

        executorService.execute(controller);
        System.out.println("Ground Control starts working...");

        try {
            listener = new ServerSocket(SERVER_PORT);
            System.out.println("Ground Control is listening...");
            while (run) {
                ConnectionHandler connection = new ConnectionHandler(listener.accept());
                if (aircraftHandlers.size() == Property.AIRCRAFT_COUNT_LIMIT) {
                    String response = JsonConverter.to(Response.noSpace());
                    connection.send(response);
                    connection.close();
                } else {
                    Request request = JsonConverter.from(connection.receive(), Request.class);
                    PlaneDTO planeToSave = PlaneDTO.of(
                            request.aircraftName,
                            LocalDateTime.now(),
                            true
                    );
                    PlaneDTO savedPlane = PlaneDAO.savePlane(planeToSave);
                    if (savedPlane.id != null) {
                        PlaneInfo planeInfo = new PlaneInfo(savedPlane.id, savedPlane.planeName);
                        AircraftHandler aircraft = new AircraftHandler(connection, planeInfo);
                        synchronized (aircraftHandlers) {
                            aircraftHandlers.add(aircraft);
                        }
                        executorService.execute(aircraft);
                    }
                }
                System.out.println("loop is running");
            }
        } catch (IOException e) {

        }


        controller.stop();
        aircraftHandlers.clear();
        System.out.println("Ground control ended");
    }

    public int getPlanesCount() {
        return aircraftHandlers.size();
    }

    public List<String> getPlanesIds() {
        synchronized (aircraftHandlers) {
            return aircraftHandlers.stream()
                    .map(AircraftHandler::getAircraftName)
                    .toList();
        }
    }

    public void freeze() {
        controller.freeze();
    }

    public void thaw() {
        controller.thaw();
    }

    public void stop() {
        try {
            listener.close();
        } catch (IOException e) {

        }
        run = false;

        System.out.println("ground stop");
    }
}
