package pl.uncleglass.airtrafficcontrol;

import org.springframework.stereotype.Component;
import pl.uncleglass.airtrafficcontrol.aircraft.AircraftGenerator;
import pl.uncleglass.airtrafficcontrol.groundcontrol.GroundControl;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Component
public class AirTrafficControlController {//TODO add logging, better class name

    private final GroundControl  groundControl = new GroundControl();
    private final AircraftGenerator aircraftGenerator = new AircraftGenerator();

    private final ExecutorService executorService = Executors.newFixedThreadPool(2, r -> new Thread(r, "air-traffic-control-controller"));
    private long startTime;
    private boolean runs;

    public boolean start() {
        if (!runs) {
            executorService.execute(groundControl);
            executorService.execute(aircraftGenerator);
            startTime = System.currentTimeMillis();
            runs = true;
        }
        return runs;
    }

    public int getPlanesCount() {
        return groundControl.getPlanesCount();
    }

    public List<String> getPlanesIds() {
        return groundControl.getPlanesIds();
    }

    public long getUpTimeInSeconds() {
        long upTimeInMillis = System.currentTimeMillis() - startTime;
        return upTimeInMillis / 1000;
    }

    public void freeze() {
        aircraftGenerator.freeze();
        groundControl.freeze();
    }

    public void thaw() {
        aircraftGenerator.thaw();
        groundControl.thaw();
    }

    public void stop() {
        aircraftGenerator.stop();
        groundControl.stop();
        runs = false;
    }
}
